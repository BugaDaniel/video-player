package com.patru20.dan.videoplayer;

import java.util.Formatter;
import java.util.Locale;

public class Util {

    // Convert millisecond to string.
    public static String millisecondsToValidTimeFormat(long milliseconds)  {

        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        mFormatBuilder.setLength(0);

        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }


    public static long convertTimeStringToMiliseconds(String time) {
        String[] times = time.split(":");
        int hours = Integer.parseInt(times[0]);
        int min = Integer.parseInt(times[1]);
        String[] ssMS = times[2].split(",");
        int sec = Integer.parseInt(ssMS[0]);
        long timeInMs = 0;
        if (hours > 0) {
            timeInMs += hours * 3600000;
        }
        if (min > 0) {
            timeInMs += min * 60000;
        }
        if (sec > 0) {
            timeInMs += sec * 1000;
        }
        return timeInMs + Integer.parseInt(ssMS[1]);
    }
}
