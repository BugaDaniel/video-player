package com.patru20.dan.videoplayer;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.Formatter;
import java.util.Locale;


public class PlayerActivity extends AppCompatActivity {

    private VideoView videoView;
    private SeekBar seekBar;
    private TextView elapsedTime;
    private TextView totalTime;
    private Toolbar toolbar;
    private Button playOrPauseButton;
    private Button backwardButton;
    private Button forwardButton;

    private Uri selectedVideoUri;
    private Uri subtitlePath;
    private int currentPosition = 0;

    private static final int SELECT_VIDEO = 420;
    private static final int SELECT_SUB = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);

        //The advantage of this approach is that unlike wake locks,
        // it doesn't require special permission, and the platform correctly manages the user moving
        // between applications, without your app needing to worry about releasing unused resources.
        //use addFlags in your activity (and only in an activity, never in a service or other app component)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        videoView = findViewById(R.id.video_view);
        seekBar = findViewById(R.id.seekBar);
        elapsedTime = findViewById(R.id.time_elapsed_txt);
        totalTime = findViewById(R.id.total_time_txt);
        toolbar = findViewById(R.id.toolbar);
        playOrPauseButton = findViewById(R.id.play_pause);
        backwardButton = findViewById(R.id.skip_backwards);
        forwardButton = findViewById(R.id.skip_forward);

        setSupportActionBar(toolbar);

        totalTime = findViewById(R.id.total_time_txt);
        toolbar = findViewById(R.id.toolbar);
    }


        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouchScreenState();
                return false;
            }
        });

        // set total time and starting elapsed time
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()  {
            @Override
            public void onPrepared(MediaPlayer mp) {
                long duration = videoView.getDuration();
                totalTime.setText(millisecondsToValidTimerFormat(duration));
                elapsedTime.setText(millisecondsToValidTimerFormat(videoView.getCurrentPosition()));
                seekBar.setMax((int)duration);
                seekBar.postDelayed(onEverySecond, 1000);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    // this is when actually seekbar has been seeked to a new position
                    videoView.seekTo(progress);
                }
            }

            // the next 2 methods are for creating thumbnail and play vid from onstoptrack
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()){
                    case R.id.open_video:
                        selectVideoFile();
                        break;
                    case R.id.open_subtitle:
                        selectSubtitle();
                        break;
                }
                return true;
            }
        });

        playOrPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(videoView.isPlaying()){
                    videoView.pause();
                    currentPosition = videoView.getCurrentPosition();
                    playOrPauseButton.setText(R.string.play);
                    //explicitly clear the flag and thereby allow the screen to turn off again,
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
                else{
                    // Play local video file.
                    if(currentPosition == 0){
                        videoView.setVideoURI(selectedVideoUri);
                    }
                    else{
                        videoView.seekTo(currentPosition);

                    }
                    videoView.start();
                    elapsedTime.postDelayed(onEverySecond, 1000);
                    playVideoState();
                }
            }
        });

        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPosition = videoView.getCurrentPosition();
                int duration = videoView.getDuration();
                // 5 seconds.
                int ADD_TIME = 5000;

                if(currentPosition + ADD_TIME < duration)  {
                    videoView.seekTo(currentPosition + ADD_TIME);
                }
            }
        });
        backwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentPosition = videoView.getCurrentPosition();
                // 5 seconds.
                int SUBTRACT_TIME = 5000;

                if(currentPosition - SUBTRACT_TIME > 0 )  {
                    videoView.seekTo(currentPosition - SUBTRACT_TIME);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.buttons, menu);
        return true;
    }

    private void selectVideoFile() {
        Intent selectVideoIntent = new Intent(Intent.ACTION_GET_CONTENT);
        selectVideoIntent.setType("video/*");
        startActivityForResult(selectVideoIntent, SELECT_VIDEO);
    }

    private void selectSubtitle(){
        Intent selectVideoIntent = new Intent(Intent.ACTION_GET_CONTENT);
        selectVideoIntent.setType("text/csv|text/plain|text/rtf");
        startActivityForResult(selectVideoIntent, SELECT_SUB);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == SELECT_VIDEO && resultCode==RESULT_OK) {
            selectedVideoUri = data.getData();
            if(selectedVideoUri != null){
                String videoFileName = selectedVideoUri.getLastPathSegment();
                toolbar.setTitle(videoFileName);
            }
        }
        else if(requestCode == SELECT_SUB && resultCode==RESULT_OK){
            subtitlePath = data.getData();
        }
    }

    private void playVideoState(){
        toolbar.setVisibility(View.GONE);
        forwardButton.setVisibility(View.GONE);
        backwardButton.setVisibility(View.GONE);
        playOrPauseButton.setVisibility(View.GONE);
        seekBar.setVisibility(View.GONE);
        totalTime.setVisibility(View.GONE);
        elapsedTime.setVisibility(View.GONE);
    }

    private void onTouchScreenState(){
        toolbar.setVisibility(View.VISIBLE);
        forwardButton.setVisibility(View.VISIBLE);
        backwardButton.setVisibility(View.VISIBLE);
        seekBar.setVisibility(View.VISIBLE);
        totalTime.setVisibility(View.VISIBLE);
        elapsedTime.setVisibility(View.VISIBLE);

        if(videoView.isPlaying()){
            playOrPauseButton.setText(R.string.pause);
        }
        else{
            playOrPauseButton.setText(R.string.play);
        }
        playOrPauseButton.setVisibility(View.VISIBLE);

        // 5 sec timer
        long delayTime = 5000;
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                playVideoState();
            }
        };
        handler.postDelayed(runnable, delayTime);
    }

    // Convert millisecond to string.
    private String millisecondsToValidTimerFormat(long milliseconds)  {

        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        mFormatBuilder.setLength(0);

        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private Runnable onEverySecond = new Runnable() {

        @Override
        public void run() {
            if(seekBar != null) {
                seekBar.setProgress(videoView.getCurrentPosition());
            }
            if(videoView.isPlaying()) {
                seekBar.postDelayed(onEverySecond, 1000);
            }
            int position = videoView.getCurrentPosition();
            String currentPos = millisecondsToValidTimerFormat(position);
            elapsedTime.setText(currentPos);
        }
    };
            }
        }
        finish();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

}
