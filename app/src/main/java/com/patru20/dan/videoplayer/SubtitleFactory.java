package com.patru20.dan.videoplayer;

import android.content.Context;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class SubtitleFactory {

    private Uri uriPath;
    private Context context;

    public static HashMap<String, Subtitle> subList = new HashMap<>();

    public SubtitleFactory(Uri uriPath, Context context){
        this.uriPath = uriPath;
        this.context = context;
    }

    public void createSubsFromUri(){

       if(uriPath != null){
           int lineNumber = 0;

           int subNumber = 0;
           long subStart = 0;
           long subEnd = 0;
           StringBuilder builder = new StringBuilder();

           BufferedReader reader = null;

           try {
               reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(context.getContentResolver().openInputStream(uriPath))));
               String line;

               while ((line = reader.readLine()) != null) {
                   lineNumber ++;

                   if(line.trim().equals("")){
                       // "" = subtitle delimiter
                       lineNumber = 0;
                       Subtitle subtitle = new Subtitle(subNumber, subStart, subEnd, builder.toString());
                       subList.put(String.valueOf(subtitle.getSubNumber()), subtitle);

                   }
                   else{
                       if(lineNumber == 1){
                           // sub number
                           subNumber = Integer.parseInt(line);
                       }
                       else if(lineNumber == 2){
                           // sub time
                           String[] subTime = line.split("-->");
                           String subStartTime = subTime[0];
                           String subEndTime = subTime[1];
                           subStart = convertDateStringToMiliseconds(subStartTime);
                           subEnd = convertDateStringToMiliseconds(subEndTime);
                       }
                       else{
                           //sub txt
                           builder.append(line);
                       }
                   }
               }

           } catch (Exception e) {
               e.printStackTrace();
           } finally {
               if (reader != null){
                   try {
                       reader.close();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }
           }
       }

    }

    private long convertDateStringToMiliseconds(String time){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        long timeInMilisec = 0;
        try {
            Date mDate = sdf.parse(time);
            timeInMilisec = mDate.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeInMilisec;
    }

}
