package com.patru20.dan.videoplayer;


public class Subtitle {

    private int subNumber;
    private Long startSub;
    private Long endSub;
    private String subText;

    public Subtitle(int subNumber, Long startSub, Long endSub, String subText ){
        this.subNumber = subNumber;
        this.startSub = startSub;
        this.endSub = endSub;
        this.subText = subText;
    }

    public Long getStartSub() {
        return startSub;
    }

    public Long getEndSub() {
        return endSub;
    }

    public int getSubNumber() {
        return subNumber;
    }

    public String getSubText() {
        return subText;
    }





}
